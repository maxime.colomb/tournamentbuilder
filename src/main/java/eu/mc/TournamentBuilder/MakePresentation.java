package eu.mc.TournamentBuilder;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.sl.usermodel.VerticalAlignment;
import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextBox;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;

import eu.mc.TournamentBuilder.games.Pool;
import eu.mc.TournamentBuilder.runner.Runner;


public class MakePresentation {


	/**
	 * Create a pptx for a more showy way to announce teams and pools (sorry, free
	 * software lovers, for that ms format)
	 *
	 * @param pools an array of {@link Pool}
	 * @throws IOException
	 */
	public static void MakePoolPresentation(Pool[] pools) throws IOException {
		XMLSlideShow ppt = new XMLSlideShow();
		XSLFSlideMaster defaultMaster = ppt.getSlideMasters().get(0);
		for (Pool p : pools) {
			XSLFSlideLayout poolName = defaultMaster.getLayout(SlideLayout.BLANK);
			XSLFSlide poolNameSlide = ppt.createSlide(poolName);
			XSLFTextBox tb = poolNameSlide.createTextBox();

			tb.setHorizontalCentered(true);
			tb.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tb.setAnchor(poolNameSlide.getBackground().getAnchor());
			XSLFTextParagraph tbText = tb.addNewTextParagraph();
			XSLFTextRun t = tbText.addNewTextRun();
			t.setText("POULE " + p.getName());
			t.setFontColor(Color.white);
			t.setBold(true);
			t.setFontSize(75.0);

			XSLFSlideLayout team = defaultMaster.getLayout(SlideLayout.BLANK);
			for (Runner r : p.getRunners()) {
				XSLFSlide slide = ppt.createSlide(team);
				slide.getBackground().setFillColor(Color.black);
				XSLFTextBox tb1 = slide.createTextBox();
				tb1.setAnchor(slide.getBackground().getAnchor());
				XSLFTextParagraph tb1Text = tb1.addNewTextParagraph();
				XSLFTextRun t1 = tb1Text.addNewTextRun();
				t1.setText("POULE " + p.getName());
				t1.setFontColor(Color.white);
				t1.setFontSize(30.0);

				XSLFTextBox tb2 = slide.createTextBox();
				tb2.setHorizontalCentered(true);
				tb2.setVerticalAlignment(VerticalAlignment.MIDDLE);
				tb2.setAnchor(slide.getBackground().getAnchor());
				XSLFTextParagraph tb2Text = tb2.addNewTextParagraph();
				XSLFTextRun r2 = tb2Text.addNewTextRun();
				r2.setText(r.toString().replace("&", "\n & \n").replace(".", " "));
				r2.setFontColor(Color.white);
				r2.setBold(true);
				r2.setFontSize(50.0);
			}

			XSLFSlide slide = ppt.createSlide(team);
			slide.getBackground().setFillColor(Color.black);
			XSLFTextBox tb1 = slide.createTextBox();
			tb1.setAnchor(slide.getBackground().getAnchor());
			tb1.setHorizontalCentered(true);
			XSLFTextParagraph tb1Text = tb1.addNewTextParagraph();
			XSLFTextRun t1 = tb1Text.addNewTextRun();
			t1.setText("RECAP POULE " + p.getName());
			t1.setFontColor(Color.white);
			t1.setFontSize(40.0);

			XSLFTextBox tb2 = slide.createTextBox();
			tb2.setHorizontalCentered(true);
			tb2.setVerticalAlignment(VerticalAlignment.MIDDLE);
			tb2.setAnchor(slide.getBackground().getAnchor());
			XSLFTextParagraph tb2Text = tb2.addNewTextParagraph();
			XSLFTextRun r2 = tb2Text.addNewTextRun();
			String recap = "";
			boolean first = true;
			for (Runner r : p.getRunners()) {
				if (first) {
					recap = r.toString().replace(".", " ").replace("&", " & ");
					first = false;
				} else
					recap = recap + "\n VS \n" + r.toString().replace(".", " ").replace("&", " & ");
			}
			r2.setText(recap);
			r2.setFontColor(Color.white);
			r2.setBold(true);
			r2.setFontSize(45.0);
		}
		FileOutputStream out = new FileOutputStream("/tmp/powerpoint.pptx");
		ppt.write(out);
		ppt.close();
		out.close();
	}
}
