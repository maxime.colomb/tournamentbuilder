package eu.mc.TournamentBuilder.runner;

/**
 * Runner are the abstract object that compete in the tournament. It could be
 * either a single player or a team of an undefinied number of players.
 * 
 * @author Maxime Colomb
 *
 */
public abstract class Runner {
	String name;

	public String toString() {
		return name;
	}
}
