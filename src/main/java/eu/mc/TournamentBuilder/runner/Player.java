package eu.mc.TournamentBuilder.runner;

/**
 * Player object represent a person, fictive or not. Email to send virtual
 * prices and congrats. Every fields can be null.
 * 
 * @author Maxime Colomb
 *
 */
public class Player extends Runner {
	private String prenom, nom, email;

	public Player(String nom, String prenom, String email) {
		this.email = email;
		this.prenom = firstLetterAllCap(prenom);
		this.nom = firstLetterAllCap(nom);
	}

	public static String firstLetterAllCap(String in) {
		if (in == null || in.equals(""))
			return "";
		return in.substring(0, 1).toUpperCase() + in.substring(1, in.length());
	}

	public boolean equals(Player playerToCompare) {
		if (playerToCompare.getNom().equals(this.nom) && playerToCompare.getEmail().equals(this.email)
				&& playerToCompare.getPrenom().equals(this.prenom))
			return true;
		return false;
	}

	public String toString() {
		String result = nom + "." + prenom + ":" + email;
		if (result.endsWith(":"))
			result = result.substring(0, result.length() - 1);
		if (result.endsWith("."))
			result = result.substring(0, result.length() - 1);
		return result;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
