package eu.mc.TournamentBuilder.runner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Team extends Runner {
	Player[] composition;
	String name;

	public Team(Player[] composition) {
		this.composition = composition;
		super.name = this.getName();
	}

	/**
	 * Generate a {@link List} of {@link Team}s from a {@link List} of
	 * {@link Player} for a well balanced {@link List} of {@link Player}
	 * 
	 * @param list       list of players
	 * @param runnerSize number of {@link Player} in each {@link Team}
	 * @return
	 */
	public static List<Runner> makeRandomTeam(List<Player> list, int runnerSize) {
		return makeRandomTeam(list, runnerSize, 0);
	}

	/**
	 * Generate a {@link List} of {@link Team}s from a {@link List} of
	 * {@link Player}. Two options are coded if the sizes of the list is not perfect
	 * (BIGTEAM and SMALLTEAM only for now).
	 * 
	 * @param list       list of players
	 * @param runnerSize number of {@link Player} in each {@link Team}
	 * @param extra      exedent of {@link Player} to make perfectly balanced teams.
	 * @return
	 */
	public static List<Runner> makeRandomTeam(List<Player> list, int runnerSize, int extra) {
		List<Runner> listResult = new ArrayList<Runner>();
		Collections.shuffle(list);
		for (int i = list.size() - 1; i >= 0; i = i - runnerSize) {
			int addToI = 0;
			int restore = runnerSize;
			if (Math.abs(extra) != 0 && i - runnerSize < Math.abs(extra)) {
				if (extra > 0) {
					// TODO improve - Doesnt work for other cases than 2 teams
					runnerSize--;
					addToI = 1;
					extra--;
				} else {
					addToI = -1;
					runnerSize++;
					extra++;
				}
			}
			Player[] players = new Player[runnerSize];
			for (int j = i; j > i - runnerSize; j--)
				players[i - j] = list.get(j);
			listResult.add(new Team(players));
			i = i + addToI;
			runnerSize = restore;
		}
		return listResult;
	}

	public Player[] getComposition() {
		return composition;
	}

	public void setComposition(Player[] composition) {
		this.composition = composition;
	}

	/**
	 * Name is the mainly characteristic of a team. If no name is manually set, we
	 * generate it from the {@link Player}'s names
	 * 
	 * @return the name
	 */
	public String getName() {
		if (name == null || name.equals("")) {
			name = "";
			for (int i = 0; i < composition.length; i++)
				name = name + "&" + composition[i].toString();
			if (name.startsWith("&"))
				name = name.substring(1, name.length());
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
