package eu.mc.TournamentBuilder.runner;

public class PlayerStat extends Player implements Comparable<PlayerStat> {

	int points, goalaverage;

	public PlayerStat(String name) {
		super(name, "", "");
	}

	public Integer getPoints() {
		return points;
	}

	public Integer getGoalaverage() {
		return goalaverage;
	}

	public void incrementGoalaverage(int score) {
		this.goalaverage = this.goalaverage + score;
	}

	public void incrementPoint() {
		this.points++;
	}

	public String toString() {
		return this.getNom() + " : points=" + this.getPoints() + " - Goalaverage=" + this.getGoalaverage();
	}

	@Override
	public int compareTo(PlayerStat arg0) {
		if (this.points == arg0.getPoints())
			return arg0.getGoalaverage().compareTo(this.goalaverage);
		return arg0.getPoints().compareTo(this.getPoints());
	}
}
