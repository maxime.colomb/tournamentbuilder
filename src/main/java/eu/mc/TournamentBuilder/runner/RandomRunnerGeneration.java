package eu.mc.TournamentBuilder.runner;

import com.opencsv.CSVReader;
import fr.ign.artiscales.tools.geoToolsFunctions.Attribute;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static eu.mc.TournamentBuilder.games.CreatePoolFinalTournament.generatePools;

/**
 * Generate a list of team from a .csv file
 *
 * @author Maxime Colomb
 */
public class RandomRunnerGeneration {
    private static IncompletePlayerNumber iPN;

//    public static void main(String[] args) throws IOException {
//        setIPN(IncompletePlayerNumber.BIGTEAM);
//        List<Runner> runners = generateRunners(new File("test.csv"), 3);
//        System.out.println(generatePools(runners));
//    }

    public static List<Runner> generateRunners(File csvFilePath, int runnerSize) throws IOException {
        // get players from a CSV File
        CSVReader csv = new CSVReader(new FileReader(csvFilePath));
        String[] firstLine = csv.readNext();
        List<Runner> listResult = new ArrayList<Runner>();
        int iNom = Attribute.getIndice(firstLine, "Nom");
        int iEmail = Attribute.getIndice(firstLine, "Email");
        int iPrenom = Attribute.getIndice(firstLine, "Prenom");
        List<Player> list = new ArrayList<Player>();
        for (String[] line : csv.readAll())
            list.add(new Player(line[iNom], line[iPrenom], line[iEmail]));
        csv.close();

        // Check for doubles
        for (Player p : list)
            for (Player pComp : list)
				if (p.equals(pComp) && p != pComp)
                    System.out.println("WARNING: doubles in the input csv file : " + p);

        // case teams are of one person,
        if (runnerSize == 1) {
            listResult.addAll(list);
            return listResult;
        }

        // Check if the number of player per teams is a multiple of the number of total
        // players
        int size = list.size();
        boolean multiple = false;
        while (size >= 0) {
            size = size - runnerSize;
			if (size == 0) {
				multiple = true;
				break;
			}
        }

        // Case of right size participant
        if (multiple)
            return Team.makeRandomTeam(list, runnerSize);
        else {
            switch (getiPN()) {
                case BIGTEAM:
                    return Team.makeRandomTeam(list, runnerSize, size);
                case SMALLTEAM:
                    return Team.makeRandomTeam(list, runnerSize, -size);
                case LISTADD:
                    System.out.println("Sup player list not implemented yet");
                    break;
                case LISTDEL:
                    System.out.println("Del player list not implemented yet");
                    break;
                default:
                    return Team.makeRandomTeam(list, runnerSize, size);
            }
        }
        return null;
    }

    public static IncompletePlayerNumber getiPN() {
        return iPN;
    }

    public static void setIPN(IncompletePlayerNumber iPN) {
        RandomRunnerGeneration.iPN = iPN;
    }
}
