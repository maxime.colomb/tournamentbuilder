package eu.mc.TournamentBuilder.runner;

/**
 * List of different options to create teams if number of player matches not
 * perfectly.
 * 
 * @author Maxime Colomb
 *
 */
public enum IncompletePlayerNumber {
	SMALLTEAM, BIGTEAM, LISTADD, LISTDEL;
}
