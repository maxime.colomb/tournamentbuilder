package eu.mc.TournamentBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVWriter;

import eu.mc.TournamentBuilder.games.Match;
import eu.mc.TournamentBuilder.games.Pool;
import eu.mc.TournamentBuilder.runner.Runner;

public class MakeMatchSheet {

	public static void generateMatchSheet(Pool pools, File outFile) throws IOException {
		Pool[] p = { pools };
		generateMatchSheet(p, outFile);
	}

	public static void generateMatchSheet(Pool[] pools, File outFile) throws IOException {
		generateMatchSheet(pools, outFile, true);
	}

	public static void generateMatchSheet(Pool[] pools, File outFile, boolean recap) throws IOException {
		CSVWriter out = new CSVWriter(new FileWriter(outFile));
		for (Pool pool : pools) {
			String[] name = { "Poule " + pool.getName() };
			out.writeNext(name);
			String[] firstLine = { "Équipe Gauche", "Équipe Droite", "Score Équipe Gauche", "Score Équipe Droite" };
			out.writeNext(firstLine);
			Match[] matches;
			if (pool.isMatchesSet())
				matches = pool.getMatches();
			else
				matches = pool.generateMatches();
			for (Match m : matches) {
				String[] line = { m.getLeft().toString(), m.getRight().toString() };
				out.writeNext(line);
			}
			String[] space = { "" };
			out.writeNext(space);
			// make summary section
			if (recap) {
				String[] nameSum = { "Team Recap" };
				out.writeNext(nameSum);
				for (Runner r : pool.getRunners()) {
					String[] line = { r.toString() };
					out.writeNext(line);
				}
				out.writeNext(space);
				out.writeNext(space);
			}

		}
		out.close();
	}
}
