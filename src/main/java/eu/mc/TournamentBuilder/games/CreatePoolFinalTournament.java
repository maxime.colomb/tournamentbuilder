package eu.mc.TournamentBuilder.games;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.mc.TournamentBuilder.MakePresentation;
import eu.mc.TournamentBuilder.runner.IncompletePlayerNumber;
import eu.mc.TournamentBuilder.runner.RandomRunnerGeneration;
import org.apache.commons.math3.exception.NullArgumentException;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import eu.mc.TournamentBuilder.MakeMatchSheet;
import eu.mc.TournamentBuilder.runner.PlayerStat;
import eu.mc.TournamentBuilder.runner.Runner;

/**
 * Class to generate a Pool/Final branch tournament type. Pools are made to facilitate a final team with power of two (1/2 final, 1/4 final, etc.). The Final part should be done by
 * hand (not implemented yet). Create a team/pool presentation and display matches in the console
 * 
 * @author Maxime Colomb
 *
 */
public class CreatePoolFinalTournament {
	public static void main(String[] args) throws IOException {
		List<Runner> teams = makeTeamPhase(new File("input.csv"), 2, IncompletePlayerNumber.BIGTEAM);
		makePools(teams);
	}
	public static List<Runner> makeTeamPhase(File input, int sizeOfTeams, IncompletePlayerNumber incompletePlayerNumber) throws IOException {
		RandomRunnerGeneration.setIPN(incompletePlayerNumber);
		List<Runner> runners = RandomRunnerGeneration.generateRunners(input, sizeOfTeams);
		System.out.println(runners);
		return runners;
	}

	public static void makePools(List<Runner> runners) throws IOException {
		List<Pool> pools = generatePools(runners);
		System.out.println(pools);
		Pool.setReturnMatch(false);
		// for (Pool p : pools)
		// p.generateMatches();
		MakePresentation.MakePoolPresentation(pools.toArray(new Pool[0]));
		File sheet = new File("/tmp/out.csv");
		MakeMatchSheet.generateMatchSheet(pools.toArray(new Pool[0]), sheet);
	}


	public static void makeFinalPhase(File count) throws IOException {
				List<List<PlayerStat>> sortedPools = countPoolScore(count);
//		List<List<PlayerStat>> sortedPools = countPoolScore(new File("src/main/resources/out.csv"));

		generateFinal(sortedPools, true, new File("/tmp/final.csv"));
	}
	public static List<List<PlayerStat>> countPoolScore(File sheetCompleted) throws IOException {
		// Read pools and make classement
		CSVReader reader = new CSVReader(new FileReader(sheetCompleted));
		boolean startIt = false;
		List<Pool> listPool = new ArrayList<Pool>();
		List<String[]> lines = new ArrayList<String[]>();
		String poolName = "";
		for (String[] line : reader.readAll()) {
			// get the new Pool name
			if (line[0].startsWith("Poule "))
				poolName = line[0].replace("Poule ", "");
			// new Pool collect
			if (line[0].startsWith("Équipe Gauche")) {
				lines = new ArrayList<String[]>();
				startIt = true;
			}
			// as we start to collect, we create an empty list
			else if (startIt)
				lines.add(line);
			// if we go till a blank space, pool scores are over and we can calculate points
			if (line[0].equals("") && startIt) {
				startIt = false;
				lines.remove(lines.size() - 1);
				listPool.add(Pool.makePoolScores(lines, poolName));
			}
		}
		List<List<PlayerStat>> sortedList = new ArrayList<List<PlayerStat>>();
		for (Pool p : listPool)
			sortedList.add(p.makePoolRank());
		reader.close();
		reader = new CSVReader(new FileReader(sheetCompleted));

		// write the score on a (new) sheet
		CSVWriter writter = new CSVWriter(
				new FileWriter(new File(sheetCompleted.getParentFile(), sheetCompleted.getName().replace(".csv", "") + "completed.csv")));
		boolean total = false;
		for (String[] line : reader.readAll()) {
			if (line[0].equals("Team Recap")) {
				total = true;
				String[] newLine = { "Team Recap", "Points", "Goalaverage" };
				writter.writeNext(newLine);
				continue;
			}
			if (line[0].equals(""))
				total = false;

			if (total) {
				PlayerStat p = searchPlayer(sortedList, new PlayerStat(line[0]));
				String[] newLine = { p.getNom(), String.valueOf(p.getPoints()), String.valueOf(p.getGoalaverage()) };
				writter.writeNext(newLine);
			} else
				writter.writeNext(line);

		}
		writter.close();
		return sortedList;
	}

	public static void generateFinal(List<List<PlayerStat>> allRunners, boolean consolante, File fileOut) throws IOException {
		int size = allRunners.size();
		System.out.println(size);
		if (size == 3) {
			// choose the last two finalist between the thirds of the pool
			Pool rattrapage = new Pool("Rattrapage");
			for (List<PlayerStat> pool : allRunners)
				rattrapage.addTeam(pool.get(2));
			rattrapage.generateMatches();
			MakeMatchSheet.generateMatchSheet(rattrapage, fileOut);
			System.out.println(
					"Fill the lately generated match sheet of a dam pool between all the 3rds, calculate points and put the pool in the former sheet");
			System.exit(0);
		} else if (size == 4) {
			makeQuarth(allRunners, fileOut.getParentFile());
		}

	}

	public static void makeQuarth(List<List<PlayerStat>> allRunners, File rootFolder) throws IOException {
		for (List<PlayerStat> pool : allRunners)
			for (int i = pool.size() - 1; i >= 2; i--)
				pool.remove(i);
		new FinalRound(4, allRunners, rootFolder);
	}

	public static List<Pool> generatePools(List<Runner> allRunners) {
		int size = allRunners.size();
		List<Pool> pools = new ArrayList<Pool>();
		if (size <= 8) {
			System.out.println("trop peu d'équipes pour faire des poules");
			// create direct 1/4 de finales
		} else if (size <= 11) {
			// create 3 pools of 3 or 4
			Pool A = new Pool("A");
			Pool B = new Pool("B");
			Pool C = new Pool("C");
			Collections.shuffle(allRunners);
			int i = 0;
			for (Runner r : allRunners) {
				i++;
				if (i % 3 == 0)
					A.addTeam(r);
				else if (i % 3 == 1)
					B.addTeam(r);
				else if (i % 3 == 2)
					C.addTeam(r);
			}
			pools = Arrays.asList(A, B, C);
		} else if (size <= 23) {
			// create 4 pools of 3 to 6
			Pool A = new Pool("A");
			Pool B = new Pool("B");
			Pool C = new Pool("C");
			Pool D = new Pool("D");
			Collections.shuffle(allRunners);
			int i = 0;
			for (Runner r : allRunners) {
				i++;
				if (i % 4 == 0)
					A.addTeam(r);
				else if (i % 4 == 1)
					B.addTeam(r);
				else if (i % 4 == 2)
					C.addTeam(r);
				else if (i % 4 == 3)
					D.addTeam(r);
			}
			pools = Arrays.asList(A, B, C, D);
		} else if (size <= 35) {
			// create 8 pools of 3 or 4 (or 5)
			Pool A = new Pool("A");
			Pool B = new Pool("B");
			Pool C = new Pool("C");
			Pool D = new Pool("D");
			Pool E = new Pool("E");
			Pool F = new Pool("F");
			Pool G = new Pool("G");
			Pool H = new Pool("H");
			Collections.shuffle(allRunners);
			int i = 0;
			for (Runner r : allRunners) {
				i++;
				if (i % 8 == 0)
					A.addTeam(r);
				else if (i % 8 == 1)
					B.addTeam(r);
				else if (i % 8 == 2)
					C.addTeam(r);
				else if (i % 8 == 3)
					D.addTeam(r);
				else if (i % 8 == 4)
					E.addTeam(r);
				else if (i % 8 == 5)
					F.addTeam(r);
				else if (i % 8 == 6)
					G.addTeam(r);
				else if (i % 8 == 7)
					H.addTeam(r);
			}
			pools = Arrays.asList(A, B, C, D, E, F, G, H);
		} else if (size <= 48) {
			// create 12 pools of 3 or 4
			Pool A = new Pool("A");
			Pool B = new Pool("B");
			Pool C = new Pool("C");
			Pool D = new Pool("D");
			Pool E = new Pool("E");
			Pool F = new Pool("F");
			Pool G = new Pool("G");
			Pool H = new Pool("H");
			Pool I = new Pool("I");
			Pool J = new Pool("J");
			Pool K = new Pool("K");
			Pool L = new Pool("L");
			Collections.shuffle(allRunners);
			int i = 0;
			for (Runner r : allRunners) {
				i++;
				if (i % 12 == 0)
					A.addTeam(r);
				else if (i % 12 == 1)
					B.addTeam(r);
				else if (i % 12 == 2)
					C.addTeam(r);
				else if (i % 12 == 3)
					D.addTeam(r);
				else if (i % 12 == 4)
					E.addTeam(r);
				else if (i % 12 == 5)
					F.addTeam(r);
				else if (i % 12 == 6)
					G.addTeam(r);
				else if (i % 12 == 7)
					H.addTeam(r);
				else if (i % 12 == 8)
					I.addTeam(r);
				else if (i % 12 == 9)
					J.addTeam(r);
				else if (i % 12 == 10)
					K.addTeam(r);
				else if (i % 12 == 11)
					L.addTeam(r);
			}
			pools = Arrays.asList(A, B, C, D, E, F, G, H, I, J, K, L);
		} else if (size <= 64) {
			// create 16 pools of 3 or 4
			Pool A = new Pool("A");
			Pool B = new Pool("B");
			Pool C = new Pool("C");
			Pool D = new Pool("D");
			Pool E = new Pool("E");
			Pool F = new Pool("F");
			Pool G = new Pool("G");
			Pool H = new Pool("H");
			Pool I = new Pool("I");
			Pool J = new Pool("J");
			Pool K = new Pool("K");
			Pool L = new Pool("L");
			Pool M = new Pool("M");
			Pool N = new Pool("N");
			Pool O = new Pool("O");
			Pool P = new Pool("P");
			Collections.shuffle(allRunners);
			int i = 0;
			for (Runner r : allRunners) {
				i++;
				if (i % 16 == 0)
					A.addTeam(r);
				else if (i % 16 == 1)
					B.addTeam(r);
				else if (i % 16 == 2)
					C.addTeam(r);
				else if (i % 16 == 3)
					D.addTeam(r);
				else if (i % 16 == 4)
					E.addTeam(r);
				else if (i % 16 == 5)
					F.addTeam(r);
				else if (i % 16 == 6)
					G.addTeam(r);
				else if (i % 16 == 7)
					H.addTeam(r);
				else if (i % 16 == 8)
					I.addTeam(r);
				else if (i % 16 == 9)
					J.addTeam(r);
				else if (i % 16 == 10)
					K.addTeam(r);
				else if (i % 16 == 11)
					L.addTeam(r);
				else if (i % 16 == 8)
					I.addTeam(r);
				else if (i % 16 == 9)
					J.addTeam(r);
				else if (i % 16 == 10)
					K.addTeam(r);
				else if (i % 16 == 11)
					L.addTeam(r);
				else if (i % 16 == 12)
					M.addTeam(r);
				else if (i % 16 == 13)
					N.addTeam(r);
				else if (i % 16 == 14)
					O.addTeam(r);
				else if (i % 16 == 15)
					P.addTeam(r);
			}
			pools = Arrays.asList(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P);
		} else
			System.out.println("too much teams");
		return pools;
	}

	public static PlayerStat searchPlayer(List<List<PlayerStat>> allRunners, PlayerStat searchPlayer) {
		for (List<PlayerStat> pool : allRunners) {
			for (PlayerStat p : pool)
				if (p.equals(searchPlayer))
					return p;
		}
		throw new NullArgumentException();
	}
	// public static boolean isMultiple(int mainNumber, int divider) {
	// while (mainNumber >= 0) {
	// mainNumber = mainNumber - divider;
	// if (mainNumber == 0)
	// return true;
	// }
	// return false;
	// }
}
