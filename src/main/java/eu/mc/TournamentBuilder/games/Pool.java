package eu.mc.TournamentBuilder.games;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.mc.TournamentBuilder.runner.PlayerStat;
import eu.mc.TournamentBuilder.runner.Runner;

/**
 * Pool object represent a set of teams which match between each other. Pool can
 * have return game depending on the tornament type.
 * 
 * @author Maxime Colomb
 *
 */
public class Pool {
	/**
	 * Name of the Pool
	 */
	String name;
	/**
	 * Array of runners
	 */
	Runner[] runners;
	/**
	 * Are return match planned ?
	 */
	static boolean returnMatch = false;
	/**
	 * List of matches
	 */
	Match[] matches;
	/**
	 * If the list of matches are exogenously set
	 */
	boolean matchesSet = false;

	public Pool(String name) {
		this.name = name;
		this.runners = new Runner[0];
	}

	public Pool(String name, Match[] matches2) {
		this.name = name;
		this.matches = matches2;
		matchesSet = true;
	}

	public String toString() {
		String out = "POULE " + name + "::";
		if (runners.length == 0)
			return "empty " + this.name + " pool";
		else
			out = out + runners[0].toString();
		for (int i = 1; i < runners.length; i++)
			out = out + "-AGAINST-" + runners[i].toString();
		return out;
	}

	/**
	 * Add a new {@link Runner} to the {@link Pool}
	 * 
	 * @param runner
	 */
	public void addTeam(Runner runner) {
		Runner[] newRunner = new Runner[runners.length + 1];
		for (int i = 0; i < runners.length; i++)
			newRunner[i] = this.runners[i];
		newRunner[runners.length] = runner;
		this.runners = newRunner;
	}

	/**
	 * Generate a list of {@link Match} between every {@link Runner} of this
	 * {@link Pool}. Take care if a return match is planned. Also print them on the
	 * console
	 * 
	 * @return an array of {@link Match}
	 */
	public Match[] generateMatches() {
		String result = "\n\n POULE " + this.name + "\n\n";
		int nbMatch = 0;
		for (int nbTeam = runners.length - 1; nbTeam >= 0; nbTeam--)
			nbMatch = nbMatch + nbTeam;
		if (returnMatch)
			matches = new Match[nbMatch * 2];
		else
			matches = new Match[nbMatch];
		int i = 0;
		for (Runner r : runners) {
			isMatch: for (Runner r2 : runners) {
				if (r == r2)
					continue;
				Match match = new Match(r, r2);
				match.setReturnMatch(returnMatch);
				if (returnMatch) {
					result = result + r + "--VS--" + r2 + "\n";
					matches[i] = new Match(r, r2);
				} else {
					// check if match already in the list
					for (Match m : matches)
						if (m != null && m.equals(match))
							continue isMatch;
					result = result + r + "--VS--" + r2 + "\n";
					matches[i] = new Match(r, r2);
				}
				i++;
			}
		}
		System.out.println(result);
		return matches;
	}

	public List<PlayerStat> makePoolRank() {
		if (runners == null || runners.length == 0
				|| Arrays.stream(runners).filter(x -> !(x instanceof PlayerStat)).count() != 0) {
			System.out.println("MakePoolRank(): uncorrect Runner for the " + this.name + " pool");
			return null;
		}
		List<PlayerStat> l = new ArrayList<PlayerStat>();
		for (Runner r : runners)
			l.add((PlayerStat) r);
		Collections.sort(l);
		System.out.println(l);
		return l;
	}

	/**
	 * For each pool, calculate the score. Must be written the good way
	 * 
	 * @param lines
	 * @return
	 */
	public static Pool makePoolScores(List<String[]> lines, String poolName) {
		Pool runners = new Pool(poolName);
		for (String[] line : lines) {
			if (line.length < 3) {
				System.out.println("Pool scores not completed. Exit program");
				System.exit(0);
			}
			PlayerStat pLeft = getContainsEqual(runners, new PlayerStat(line[0]));
			PlayerStat pRight = getContainsEqual(runners, new PlayerStat(line[1]));
			Integer ptLeft = Integer.valueOf(line[2]);
			Integer ptRight = Integer.valueOf(line[3]);
			if (ptLeft > ptRight)
				pLeft.incrementPoint();
			else
				pRight.incrementPoint();
			pLeft.incrementGoalaverage(ptLeft - ptRight);
			pRight.incrementGoalaverage(ptRight - ptLeft);
			runners = addIfAbsent(pRight, runners);
			runners = addIfAbsent(pLeft, runners);
		}
		return runners;
	}

	public static PlayerStat getContainsEqual(Pool runners, PlayerStat runner) {
		for (Runner r : runners.getRunners())
			if (((PlayerStat) r).equals(runner))
				return ((PlayerStat) r);
		return runner;
	}

	public static Pool addIfAbsent(PlayerStat playerStat, Pool pool) {
		for (Runner p : pool.getRunners())
			if (((PlayerStat) p).equals(playerStat))
				return pool;
		pool.addTeam(playerStat);
		return pool;
	}

	public static boolean isReturnMatch() {
		return returnMatch;
	}

	public static void setReturnMatch(boolean doReturnMatch) {
		returnMatch = doReturnMatch;
	}

	public String getName() {
		return name;
	}
	
	public Match[] getMatches() {
		return matches;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public Runner[] getRunners() {
		return runners;
	}
	
	public boolean isMatchesSet() {
		return matchesSet;
	}
}
