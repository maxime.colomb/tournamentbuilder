package eu.mc.TournamentBuilder.games;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.mc.TournamentBuilder.MakeMatchSheet;
import eu.mc.TournamentBuilder.runner.Player;
import eu.mc.TournamentBuilder.runner.PlayerStat;
import eu.mc.TournamentBuilder.runner.Runner;

public class FinalRound {
	String name;
	Match[] matches;
	List<List<PlayerStat>> poolPlayers;

	public FinalRound(int round, List<List<PlayerStat>> poolPlayers, File rootFolder) throws IOException {
		this.poolPlayers = poolPlayers;
		this.name = makeName(round);
		int size = poolPlayers.size();
		if (size != round || isPowerOf2(round))
			System.out.println("Final Pool" + this.name + ", incorrect number of player with " + poolPlayers);
		generateFinalMatches(round, new File(rootFolder, "final.csv"));
	}

	public static String makeName(int round) {
		return "1/" + round + "th";
	}

	/**
	 * Generate a list of {@link Match} between every {@link Runner} of this
	 * {@link Pool}. Take care if a return match is planned. Also print them on the
	 * console
	 * 
	 * @return an array of {@link Match}
	 * @throws IOException
	 */
	public void generateFinalMatches(int round, File outFile) throws IOException {
		List<Pool> pools = new ArrayList<Pool>();
		matches = new Match[round];
		for (int i = 0; i < round; i++) {
			int j = round / 2 + i;
			if (j >= round)
				j = j - round;
			matches[i] = new Match(new Player(poolPlayers.get(i).get(0).getNom(),"",""), new Player(poolPlayers.get(j).get(1).getNom(),"",""));
		}
		pools.add(new Pool(makeName(round), matches));
//		do {
//			round = round / 2;
//			matches = new Match[round];
//			for (int i = 1; i <= round; i++) {
//				int j = round / 2 + i;
//				if (j > round)
//					j = j - round;
//				//Zob, make a tree
////				matches[i] = new Match(new Player(poolPlayers.get(i).get(0) + "OR"+ poolPlayers.get(j).get(1),"",""));
//			}
//
//		} while (round >= 1);
		MakeMatchSheet.generateMatchSheet(pools.toArray(new Pool[0]), outFile, false);
	}

	public boolean isPowerOf2(int number) {
		if (number > 1)
			if (number / 2 == 1)
				return true;
			else if (number / 2 > 2)
				return isPowerOf2(number / 2);
		return false;
	}
}
