package eu.mc.TournamentBuilder.games;

import org.apache.commons.lang3.tuple.ImmutablePair;

import eu.mc.TournamentBuilder.runner.Runner;

/**
 * Match object consist of a game between two runners, with a possible return
 * game.
 * 
 * @author Maxime Colomb
 *
 */
public class Match {
	/**
	 * match
	 */
	ImmutablePair<Runner, Runner> match;
	boolean returnMatch = false;

	public Match(Runner r1, Runner r2) {
		this.match = new ImmutablePair<Runner, Runner>(r1, r2);
	}

	public boolean isReturnMatch() {
		return returnMatch;
	}

	public void setReturnMatch(boolean returnMatch) {
		this.returnMatch = returnMatch;
	}

	public Runner getLeft() {
		return match.getLeft();
	}

	public Runner getRight() {
		return match.getRight();
	}

	/**
	 * Compare a match to this object to see if they are the same.
	 * <ul>
	 * <li>If a return match is planned, compare if the position of the teams are in
	 * the same order (saying that the first player is receiving)</li>
	 * <li>If no return match is planned, compare regardless of the position</li>
	 * </ul>
	 * 
	 * @param otherMatch match to compare
	 */
	public boolean equals(Match otherMatch) {
		if (returnMatch)
			if (otherMatch.getLeft() == match.getLeft() && otherMatch.getRight() == match.getRight())
				return true;
			else
				return false;
		else if ((otherMatch.getLeft() == match.getLeft() || otherMatch.getRight() == match.getLeft())
				&& (otherMatch.getLeft() == match.getRight() || otherMatch.getRight() == match.getRight()))
			return true;
		else
			return false;
	}
	
	public String toString() {
		return this.getLeft() + "-VS-" + this.getRight();
	}
}
