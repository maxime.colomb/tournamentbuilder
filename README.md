# Génerer un tournoi
Tout se passe dans la classe games.CreatePoolFinalTournament
## Créer des équipes aléatoires

Par défaut, crée des équipes aléatoires d'une taille prédéfinie.

Si vous avez déjà des équipes constitués, hackez la vie, mettez des équipes de 1 et les équipes déjà constituées dans votre .csv.
Sinon :

1. Indiquez le chemin de votre .csv d'entrée dans le premier argument. Le .csv d'entrée doit avoir le header suivant mais ses champs peuvent être vide (c'est nul mais si vous êtes pas content, rendez ces champs facultatifs)
   *'''Nom,Email,Prenom'''*
2. Renseignez le nombre de personnes par poule
3. Renseignez le comportement souhaité si les équipes doivent sont désequilibrées (soit une plus grosse équipe, soit une plus petite équipe).
    1. ***IncompletePlayerNumber.SMALLTEAM*** : crée une plus petite équipe
    2. ***IncompletePlayerNumber.BIGTEAM***: crée une plus grosse équipe
    3. ***IncompletePlayerNumber.LISTADD***: Utilise une liste complémentaire de personne à ajouter aléatoirement pour compléter les équipes (*à implémenter, tas de fainéant*)
    4. ***IncompletePlayerNumber.LISTDEL***: Utilise une liste complémentaire de personne présentes dans le .csv des joueurs à retirer aléatoirement pour compléter les équipes (*à implémenter, tas de fainéant*)


## Créer les poules d'un tournoi

Ça le fait tout seul. Le nombre de poules est fixé en fonction du nombre d'équipes pour que ça fasse des belles poules bien dodues mais pas trop (un végétarien écrit ces lignes)
Si vous trouvez qu'elles sont pas belles mes poules, faites vous plaisir avec une nouvelle recette.
Deux fichiers sont générés dans votre */tmp* . L'un présente les poules (et si c'est surprise, les équipes). L'autre a les feuilles de match qui servirons à générer la phase finale.

## Créer la phase finale

Description is coming, mais dans mes souvenirs, ça marche.
Ça se passe avec la classe makeFinalPhase(File count).
